<?php
/**
 * Created by PhpStorm.
 * User: STS
 * Date: 07/08/2020
 * Time: 13:37
 */
namespace MauticPlugin\MauticYetiforceBundle\Integration;
use Mautic\IntegrationsBundle\Integration\BasicIntegration;
use Mautic\IntegrationsBundle\Integration\ConfigurationTrait;
use Mautic\IntegrationsBundle\Integration\Interfaces\BasicInterface;

class MauticYetiforceIntegration extends BasicIntegration implements BasicInterface
{
    use ConfigurationTrait;

    public const NAME         = 'mauticyetifore';
    public const DISPLAY_NAME = 'Mautic Yetifore';

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return self::DISPLAY_NAME;
    }

    public function getIcon(): string
    {
        return 'plugins/MauticYetiforceBundle/Assets/img/logo.png';
    }
}
