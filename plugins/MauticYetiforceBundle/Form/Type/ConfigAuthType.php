<?php

declare(strict_types=1);

namespace MauticPlugin\MauticYetiforceBundle\Form\Type;

use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConfigAuthType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'client_id',
            TextType::class,
            [
                'label' => 'mauticyetifore.token',
                'label_attr' => ['class' => 'control-label'],
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ],
            ]
        );
    }

    public function configureOptions(OptionsResolver $optionsResolver): void
    {
        $optionsResolver->setDefaults(
            [
                'integration' => null,
            ]
        );
    }
}
