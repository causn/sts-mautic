<?php
/**
 * Created by PhpStorm.
 * User: STS
 * Date: 07/08/2020
 * Time: 15:59
 */

namespace MauticPlugin\MauticYetiforceBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class MauticYetiforceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('token', TextType::class)
            ->add('save', SubmitType::class)
        ;
    }


}